# romawi-ke-desimal

Oracle function untuk merubah angka romawi ke desimal

## Create function

```
create or replace function romawi_ke_desimal(sr varchar2)
return number deterministic is
  des number := 0;
begin
  for i in 1..regexp_count(sr, '(CM|M|CD|D|XC|C|XL|L|IX|X|IV|V|I)') loop
    des := des +
      case regexp_substr(sr, '(CM|M|CD|D|XC|C|XL|L|IX|X|IV|V|I)', 1, i)
        when 'M'  then 1000
        when 'CM' then 900
        when 'D'  then 500
        when 'CD' then 400
        when 'C'  then 100
        when 'XC' then 90
        when 'L'  then 50
        when 'XL' then 40
        when 'X'  then 10
        when 'IX' then 9
        when 'V'  then 5
        when 'IV' then 4
        when 'I'  then 1
      end;
  end loop;

  return des;
end;
/
```


